use clap::{Args, Parser, Subcommand};
use core::tell_server;
use std::env::VarError;
use std::io::Write;

const PACKAGE_NAME: &str = env!("CARGO_PKG_NAME");

#[derive(Subcommand, Debug)]
enum Action {
    /// serves autoqueue
    #[clap(about)]
    Serve,
    /// interact with the system
    #[clap(about)]
    System(ReviewTarget),
    /// tell server a message
    #[clap(about)]
    Tell { args: Vec<String> },
}

#[derive(Debug, Clone, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct ReviewTarget {
    #[command(subcommand)]
    command: ReviewTargetList,
}

#[derive(Debug, Clone, Subcommand)]
pub enum ReviewTargetList {
    /// edit the daemon config file
    #[clap(about)]
    Config,
    /// view systemctl status
    #[clap(about)]
    Status,
    /// installs systemctl service
    #[clap(about)]
    Install,
    /// reloads daemon
    #[clap(about)]
    Reload,
}

/// A task queuing implementation
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(subcommand)]
    action: Action,
    #[clap(short, long)]
    socket: Option<String>,
}

fn serve(path: &str) {
    println!("{PACKAGE_NAME}: serving ({path})");
    if let Err(e) = core::serve(path) {
        eprintln!("{e}");
        std::process::exit(1);
    }
    println!("\n{PACKAGE_NAME}: exiting ({path})");
}

fn resolve_socket_name() -> String {
    format!("{PACKAGE_NAME}_SOCKET").to_uppercase()
}

fn resolve_socket_path() -> Result<String, String> {
    let socket = resolve_socket_name();

    match std::env::var(&socket) {
        Ok(path) => Ok(path),
        Err(VarError::NotPresent) => Err("Missing variable".to_string()),
        Err(e) => Err(format!(
            "Unknown error while attempting to read {socket} environment variable: {e}"
        )),
    }
}

fn resolve_xdg_socket_path() -> Result<String, String> {
    match std::env::var("XDG_RUNTIME_DIR") {
        Ok(path) => Ok(format!("{path}/{}.sock", PACKAGE_NAME.to_lowercase())),
        Err(VarError::NotPresent) => {
            Err("Missing environment variable XDG_RUNTIME_DIR".to_string())
        }
        Err(e) => Err(format!(
            "Unknown error while attempting to read XDG_RUNTIME_DIR environment variable: {e}"
        )),
    }
}

fn guess_daemon_config() -> (String, String) {
    let config_folder = std::env::var("XDG_CONFIG_HOME")
        .unwrap_or_else(|_| format!("{}/.config", std::env::var("HOME").unwrap()));

    let systemd_config = format!("{config_folder}/systemd/user");
    (
        systemd_config.to_string(),
        format!("{systemd_config}/{PACKAGE_NAME}.service"),
    )
}

fn make_systemd_content() -> String {
    format!(
        r#"[Unit]
Description={PACKAGE_NAME} server daemon
After=network-online.target
StartLimitIntervalSec=0

[Service]
Type=simple
ExecStart={PACKAGE_NAME} serve
Restart=always
RestartSec=1

[Install]
WantedBy=multi-user.target
"#
    )
}

fn overwrite_file(folder: &str, path: &str, content: &str) {
    std::fs::create_dir_all(folder).unwrap_or_else(|_| panic!("couldn't mkdir {folder}"));
    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path)
        .unwrap_or_else(|_| panic!("Could not create file {path}"));
    f.write_all(content.as_bytes()).unwrap();
    f.flush().unwrap();
}

fn install() -> Result<(), String> {
    let (config_folder, config_file) = guess_daemon_config();
    let config_file_content = make_systemd_content();

    overwrite_file(&config_folder, &config_file, &config_file_content);

    let service = format!("{PACKAGE_NAME}.service");

    println!();
    println!("systemd service has been written in {config_file}");
    println!("make sure to reset the systemd daemon, enable, start and review the status with");
    println!();
    println!("systemctl --user daemon-reload");
    println!("systemctl --user enable {service}");
    println!("systemctl --user start {service}");
    println!("systemctl --user status {service}");

    Ok(())
}

fn run_inherited_cmd(cmd: &str, args: &[&str]) {
    let _err = std::process::Command::new(cmd)
        .args(args)
        .stderr(std::process::Stdio::inherit())
        .stdout(std::process::Stdio::inherit())
        .output();
}

fn main() {
    let cli = Cli::parse();

    let socket_path = if let Some(socket_path) = cli.socket {
        socket_path
    } else {
        match resolve_socket_path() {
            Ok(path) => path,
            Err(e1) => match resolve_xdg_socket_path() {
                Ok(path) => path,
                Err(e2) => {
                    eprintln!("{PACKAGE_NAME}: could not locate socket :\n    {e1}\n    {e2}");
                    std::process::exit(1);
                }
            },
        }
    };

    match cli.action {
        Action::Serve => {
            serve(&socket_path);
        }
        Action::System(target) => match target.command {
            ReviewTargetList::Config => {
                let (_, config_file) = guess_daemon_config();
                run_inherited_cmd("editor", &[&config_file]);
            }
            ReviewTargetList::Status => run_inherited_cmd(
                "systemctl",
                &["--user", "status", &format!("{PACKAGE_NAME}.service")],
            ),
            ReviewTargetList::Install => match install() {
                Ok(_) => {}
                Err(e) => {
                    eprintln!("{PACKAGE_NAME} error: {e}");
                    std::process::exit(1);
                }
            },
            ReviewTargetList::Reload => {
                run_inherited_cmd(
                    "systemctl",
                    &[
                        "--user",
                        "disable",
                        "--now",
                        &format!("{PACKAGE_NAME}.service"),
                    ],
                );
                run_inherited_cmd("systemctl", &["--user", "daemon-reload"]);
                run_inherited_cmd(
                    "systemctl",
                    &[
                        "--user",
                        "enable",
                        "--now",
                        &format!("{PACKAGE_NAME}.service"),
                    ],
                )
            }
        },
        Action::Tell { args } => {
            let pwd = std::env::current_dir()
                .expect("Could not read current dir")
                .into_os_string()
                .into_string()
                .expect("conversion error");

            let half_owned: Vec<_> = args.iter().map(String::as_str).collect();

            let _ = tell_server(&socket_path, &half_owned, &pwd);
        }
    }
}
