use std::{
    fmt::{Display, Formatter},
    sync::mpsc::SendError,
};

use crate::Command;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub struct Error {
    pub what: String,
    pub file: String,
    pub line: u32,
}

impl Error {
    #[must_use]
    const fn new(what: String, file: String, line: u32) -> Self {
        Self { what, file, line }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.what)
    }
}

impl std::error::Error for Error {}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        let location = std::panic::Location::caller();
        Error::new(
            value.to_string(),
            location.file().to_string(),
            location.line(),
        )
    }
}

impl From<SendError<Command>> for Error {
    fn from(value: SendError<Command>) -> Self {
        let location = std::panic::Location::caller();
        Error::new(
            value.to_string(),
            location.file().to_string(),
            location.line(),
        )
    }
}
