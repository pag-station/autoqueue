mod error;

use std::io::{Read, Write};
use std::os::unix::net::{UnixListener, UnixStream};
use std::str::FromStr;
use std::sync::mpsc::{channel, TryRecvError};
use std::thread;
use std::time::Duration;

#[derive(Debug)]
pub struct Command {
    bin: String,
    args: Vec<String>,
    pwd: String,
}

impl FromStr for Command {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.lines();

        let pwd = match iter.next() {
            Some(pwd) => pwd.to_string(),
            None => {
                return Err(());
            }
        };

        let bin = match iter.next() {
            Some(bin) => bin.to_string(),
            None => {
                return Err(());
            }
        };

        let args = iter.map(ToString::to_string).collect::<Vec<String>>();

        Ok(Self { bin, args, pwd })
    }
}

struct CleanUp<'a> {
    path: &'a str,
}

impl<'a> Drop for CleanUp<'a> {
    fn drop(&mut self) {
        let _ = std::fs::remove_file(self.path);
    }
}

pub fn serve(path: &str) -> error::Result<()> {
    let (stop_server_tx, stop_server_rx) = channel::<()>();

    ctrlc::set_handler(move || {
        let _ = stop_server_tx.send(());
    })
    .expect("Error setting Ctrl-C handler");

    const TICK: Duration = Duration::from_millis(200);

    let (tx, rx) = channel::<Command>();

    let path = path.to_string();

    let thread = thread::spawn(move || {
        let _guard = CleanUp { path: &path };

        let listener = UnixListener::bind(&path).expect("could not start unix socket");

        listener
            .set_nonblocking(true)
            .expect("could not set unix socket to non blocking");

        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {
                    let mut content = String::new();
                    if stream.read_to_string(&mut content).is_err() {
                        continue;
                    }
                    if let Ok(cmd) = content.parse::<Command>() {
                        if tx.send(cmd).is_err() {
                            break;
                        }
                    };
                }
                Err(e) => {
                    if e.kind() != std::io::ErrorKind::WouldBlock {
                        break;
                    }
                }
            }
            std::thread::sleep(TICK);
            if matches!(
                stop_server_rx.try_recv(),
                Ok(_) | Err(TryRecvError::Disconnected)
            ) {
                break;
            }
        }
    });

    loop {
        if thread.is_finished() {
            break;
        }
        match rx.try_recv() {
            Ok(Command { bin, pwd, args }) => {
                println!("running command {bin} {args:?} in {pwd}");
                let _err = std::process::Command::new(bin)
                    .current_dir(pwd)
                    .args(args)
                    .stderr(std::process::Stdio::inherit())
                    .stdout(std::process::Stdio::inherit())
                    .output();
            }
            Err(TryRecvError::Empty) => {}
            Err(e) => {
                panic!("meh {e:?}")
            }
        }
        std::thread::sleep(TICK);
    }

    Ok(())
}

pub fn tell_server(path: &str, cmd: &[&str], pwd: &str) -> error::Result<()> {
    let mut stream = UnixStream::connect(path)?;
    stream.write_all(format!("{pwd}\n{}", cmd.join("\n")).as_bytes())?;
    Ok(())
}
