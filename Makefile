.PHONY: help
help: ## displays this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_\/\.-]+:.*?##/ { printf "  \033[36m%-30s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

/usr/local/bin/autoqueue: target/release/autoqueue
	sudo cp -f $< $@

target/release/autoqueue: $(shell find autoqueue) $(shell find core) Cargo.toml Cargo.lock
	cargo build --release
